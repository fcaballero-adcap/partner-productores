package banza.partner.productores;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.rdsdata.AWSRDSData;
import com.amazonaws.services.rdsdata.AWSRDSDataClientBuilder;
import com.amazonaws.services.rdsdata.model.ExecuteStatementRequest;
import com.amazonaws.services.rdsdata.model.ExecuteStatementResult;
import com.amazonaws.services.rdsdata.model.Field;

import banza.partner.productores.dto.ProductoresDto;
import banza.partner.productores.dto.ProductoresResponse;

public class HandlerProductores implements RequestHandler<Map<String, String>, ProductoresResponse> {

	@Override
	public ProductoresResponse handleRequest(Map<String, String> event, Context context) {
		AWSRDSData rdsData = AWSRDSDataClientBuilder.standard()
				.withEndpointConfiguration(
						new AwsClientBuilder.EndpointConfiguration(System.getenv("ENDPOINT"), System.getenv("REGION")))
				.build();
		ExecuteStatementRequest request = new ExecuteStatementRequest().withResourceArn(System.getenv("RESOURCE_ARN"))
				.withSecretArn(System.getenv("SECRET_ARN")).withDatabase(System.getenv("BD"))
				.withSql(System.getenv("QUERY"));

		ExecuteStatementResult result = rdsData.executeStatement(request);
		List<ProductoresDto> listProd = new ArrayList<ProductoresDto>();
		if (!result.getRecords().isEmpty()) {
			for (List<Field> fields : result.getRecords()) {
				ProductoresDto prod = new ProductoresDto();
				prod.setIdProductor(fields.get(0).getLongValue());
				prod.setNombreCompleto(fields.get(1).getStringValue());
				prod.setComision(fields.get(2).getLongValue());
				listProd.add(prod);
			}
		}
		ProductoresResponse response = new ProductoresResponse();
		response.setProductores(listProd);
		return response;
	}
}