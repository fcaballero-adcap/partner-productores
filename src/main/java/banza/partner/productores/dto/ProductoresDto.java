package banza.partner.productores.dto;

public class ProductoresDto {

	private Long idProductor;
	private String nombreCompleto;
	private Long comision;

	public Long getIdProductor() {
		return idProductor;
	}

	public String getNombreCompleto() {
		return nombreCompleto;
	}

	public Long getComision() {
		return comision;
	}

	public void setIdProductor(Long idProductor) {
		this.idProductor = idProductor;
	}

	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}

	public void setComision(Long comision) {
		this.comision = comision;
	}

}