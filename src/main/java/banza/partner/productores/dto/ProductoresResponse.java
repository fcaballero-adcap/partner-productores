package banza.partner.productores.dto;

import java.util.List;

public class ProductoresResponse {

	private List<ProductoresDto> productores;

	public List<ProductoresDto> getProductores() {
		return productores;
	}

	public void setProductores(List<ProductoresDto> productores) {
		this.productores = productores;
	}
}